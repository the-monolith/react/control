interface IState {
  [K: string]: any
}

interface IActions {
  [K: string]: any
}

export const control = {
  state: <TState extends IState>(initialState: TState) => ({
    actions: <TActions extends IActions>(actionStubs: TActions) => {
      const remoteActions: Partial<TActions> = {}

      const buildActionsFromStubs = (): TActions => {
        const actions: Partial<TActions> = {}

        Object.keys(actionStubs).forEach(key => {
          actions[key] = (...args: any[]) => {
            const action = remoteActions[key]
            if (!action) {
              throw new Error(`control.actions.${key}() is not ready, use control.sync() first`)
            }
            action(...args)
          }
        })

        return actions as TActions
      }

      const controlInstance: {
        actions: TActions,
        ready: boolean,
        state: TState,
        sync(state: TState, actions: TActions): void
      } = {
        actions: buildActionsFromStubs(),
        get ready() {
          return Object.keys(remoteActions).length > 0
        },
        state: Object.assign({}, initialState), // tslint:disable-line:prefer-object-spread
        sync(state: TState, actions: TActions) {
          Object.keys(state).forEach(key => {
            controlInstance.state[key] = state[key]
          })
          Object.keys(actions).forEach(key => {
            remoteActions[key] = actions[key]
          })
        }
      }

      return controlInstance
    }
  })
}
